/* SPDX-FileCopyrightText: 2023 Johannes Strelka-Petz <johannes_at_oskars.org> for BSVWNB/Define
   SPDX-License-Identifier: CERN-OHL-S-2.0+

   This work is based on braille.scad CC-BY-4.0, 2022, Johannes Strelka-Petz, from [whistle braille](https://www.thingiverse.com/thing:5420519)
*/

/* Changes
   2024 Johannes Strelka-Petz <johannes_at_oskars.org>
   2024-09 multi-digit support; remove comments; bigger and flatter dots
*/

dot_size=2.4;
raise=-0.25;
dot_height=1.2;
dot_dist=2.5;
cell_dot_dist0=6;
cell_dot_dist=cell_dot_dist0-dot_dist;
cell_dot_dist0_vert=10;
cell_dot_dist_vert=cell_dot_dist0_vert-dot_dist*2;
additional_distance_between_cells=.4;
cell_length=cell_dot_dist+dot_dist;
cell_height=cell_dot_dist_vert+dot_dist*2;
grid_width=0.6;
grid_height=0.3;
shape="cylinder"; //"sphere"
//shape="sphere";

braillealphabet=[
     ["a","1"],
     ["b","12"],
     ["c","14"],
     ["d","145"],
     ["e","15"],
     ["f","124"],
     ["g","1245"],
     ["h","125"],
     ["i","24"],
     ["j","245"],
     ["k","13"],
     ["l","123"],
     ["m","134"],
     ["n","1345"],
     ["o","135"],
     ["p","1234"],
     ["q","12345"],
     ["r","1235"],
     ["s","234"],
     ["t","2345"],
     ["u","136"],
     ["v","1236"],
     ["w","2456"],
     ["x","1346"],
     ["y","13456"],
     ["z","1356"],
     ["ä","345"],
     ["ö","246"],
     ["ü","1256"],
     ["ß","2346"],
     [",","2"],
     [";","23"],
     [":","25"],
     ["?","26"],
     ["!","235"],
     ["(","2356"],
     [")","2356"],
     [".","3"],
     ["-","36"],
     ["§","346"],
     ["/","5","2"],
     ["&","5","136"],
     ["\\","4","35"],
     ["@","4","345"],
     ["_","4","456"],
     ["€","4","15"],
     [" ",""],
     ["1","1"], // "3456"
     ["2","12"], // "3456"
     ["3","14"], // "3456"
     ["4","145"], // "3456"
     ["5","15"], // "3456"
     ["6","124"], // "3456"
     ["7","1245"], // "3456"
     ["8","125"], // "3456"
     ["9","24"], // "3456"
     ["0","245"], // "3456"
     ["#","3456"],
     ];

// not yet implemented
vollschrift=[
     ["au","16"],
     ["eu","126"],
     ["ei","146"],
     ["ch","1456"],
     ["sch","156"],
     ["st","23456"],
     ["äu","34"],
     ["ie","346"],
     ];

function brailleentry(letter) = braillealphabet[search(letter,braillealphabet)[0]];
function braille(letter) = [for (o=[1:1:len(brailleentry(letter))-1]) brailleentry(letter)[o]];
function char_is_number_start(c) = (ord(c) >= ord("0") && ord(c) <= ord("9")) ;
function char_is_number(c) = (ord(c) >= ord("0") && ord(c) <= ord("9")) || c=="," || c=="." ;


function braille_signed(letter) =
  char_is_number_start(letter) ?
  concat(braille("#"), braille(letter)) :
  braille(letter);

function braillerow(string, i=0, braillerow) =
  i == len(string) ? braillerow :
  i == 0 ? braillerow(string, i+1, braille_signed(string[i])) :
  (!char_is_number_start(string[i-1]) && char_is_number_start(string[i])) ? braillerow(string, i+1, concat(braillerow, braille("#"), braille(string[i]))) : // "a1", char before num, insert num sign
  (char_is_number_start(string[i-1]) && !char_is_number(string[i]) && string[i]!=" ") ? braillerow(string, i+1, concat(braillerow, "6", braille(string[i]))) : // "1a", num before char, insert dot-6  
  braillerow(string, i+1, concat(braillerow, braille(string[i])));

module dot(shape=shape, dot_size=dot_size, raise=raise){
     //cylinder(h=dot_height,d1=dot_size,d2=dot_size/2);
  $fn=10;
  overlap=0.2;
  difference(){
    if(shape=="sphere"){
      union(){
	translate([0,0,-overlap]){cylinder(d=dot_size,h=raise+overlap);}
	intersection(){
	  translate([0,0,raise]){sphere(d=dot_size);}
	  cube([3,3,1.6],center=true);
	}
      }
    }
    else if(shape=="cylinder"){
      hull(){
	translate([0,0,-overlap]){cylinder(d=dot_size,h=raise+overlap);}
	translate([0,0,raise]){cylinder(d=dot_size-0.6,h=dot_size/2-0.06);}
      }
    }
    //sphere(d=dot_size,$fn=10);
    translate([0,0,-(dot_size+1.0)]){
      cylinder(d=dot_size+1,h=dot_size+1);
    }
  }
}
//dot();

module cell_array(one=false,two=false,three=false,four=false,five=false,six=false, shape=shape, dot_size=dot_size, raise=raise){
     translate([cell_dot_dist/2,cell_dot_dist_vert/2,0]){
	  if(one){
	       translate([0,dot_dist*2,0])dot(shape=shape, dot_size=dot_size, raise=raise);
	  }
	  if(two){
	       translate([0,dot_dist,0])dot(shape=shape, dot_size=dot_size, raise=raise);
	  }
	  if(three){
	       translate([0,0,0])dot(shape=shape, dot_size=dot_size, raise=raise);
	  }
	  translate([dot_dist,0,0]){
	  if(four){
	       translate([0,dot_dist*2,0])dot(shape=shape, dot_size=dot_size, raise=raise);
	  }
	  if(five){
	       translate([0,dot_dist,0])dot(shape=shape, dot_size=dot_size, raise=raise);
	  }
	  if(six){
	       translate([0,0,0])dot(shape=shape, dot_size=dot_size, raise=raise);
	  }
     }	  
     }
}
//cell_array(0,1,1,1,1,1);

module cell_dots(dots="",shape=shape, dot_size=dot_size, raise=raise){
     for(i=dots){	  
	  if(i=="1"){
	       translate([cell_dot_dist/2,cell_dot_dist_vert/2+dot_dist*2,0])dot(shape=shape, dot_size=dot_size, raise=raise);
	  }
	  if(i=="2"){
	       translate([cell_dot_dist/2,cell_dot_dist_vert/2+dot_dist,0])dot(shape=shape, dot_size=dot_size, raise=raise);
	  }
	  if(i=="3"){
	       translate([cell_dot_dist/2,cell_dot_dist_vert/2,0])dot(shape=shape, dot_size=dot_size, raise=raise);
	  }
	  if(i=="4"){
	       translate([cell_dot_dist/2+dot_dist,cell_dot_dist_vert/2+dot_dist*2,0])dot(shape=shape, dot_size=dot_size, raise=raise);
	  }
	  if(i=="5"){
	       translate([cell_dot_dist/2+dot_dist,cell_dot_dist_vert/2+dot_dist,0])dot(shape=shape, dot_size=dot_size, raise=raise);
	  }
	  if(i=="6"){
	       translate([cell_dot_dist/2+dot_dist,cell_dot_dist_vert/2,0])dot(shape=shape, dot_size=dot_size, raise=raise);
	  }
     }
     //color("green")cube([cell_length,cell_height,0.2]);
}
//cell_dots("12346");

module row(cells=[],additional_distance_between_cells=additional_distance_between_cells, shape=shape, dot_size=dot_size, raise=raise){
     for(i=[0:1:len(cells)-1]){
       translate([i*(cell_length+additional_distance_between_cells),0,0]){
	 cell_dots(cells[i],shape=shape, dot_size=dot_size, raise=raise);
       }
     }
}

function row_width(string, additional_distance_between_cells=additional_distance_between_cells) = len(string)*(cell_length+additional_distance_between_cells);
function row_height() = cell_height;

module field(rows=[], additional_distance_between_cells=additional_distance_between_cells, shape=shape, dot_size=dot_size, raise=raise){
     for(i=[0:1:len(rows)-1]){
	  translate([0,(len(rows)-1-i)*cell_height,0]){
	    row(rows[i],additional_distance_between_cells=additional_distance_between_cells, shape=shape, dot_size=dot_size, raise=raise);
	  }
     }
}

function field_height(rows)=len(rows)*cell_height;

function field_width(rows, additional_distance_between_cells=additional_distance_between_cells)= max([for (r=rows) each len(r)])*(cell_length+additional_distance_between_cells);

module textfield(shape=shape, dot_size=dot_size, raise=raise){
     field(rows=[
		 braillerow("oskar")],shape=shape, dot_size=dot_size, raise=raise);
}
//textfield();

