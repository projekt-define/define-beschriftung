/* SPDX-FileCopyrightText: 2023 Johannes Strelka-Petz <johannes_at_oskars.org> for BSVWNB/Define
   SPDX-License-Identifier: CERN-OHL-S-2.0+
*/

/* Changes
   2024 - 2025 Johannes Strelka-Petz <johannes_at_oskars.org>
   2024-11 customize dots with Punkt_Durchmesser, Punkt_Erhoehung
   2024-05 Euro Symbol, strtoupper, Linebreak
   2025-01 Colorselection for preview
*/

// in Kleinbuchstaben; Zeilenumbruch aus der Zwischenablage für den Customizer oder mit \n im Skript wird berücksichtigt
BrailleText="nicht berühren"; //"do not\ntouch";
ProfilText_="NICHT BERÜHREN"; //"DO NOT\NTOUCH";
ProfilText_ist_BrailleText_Grosz=true; //false|true
Oese=false;
GrundPlattenStaerke=1.2; //[0:0.1:2.0]
ProfilSchriftgroesze=14;
Profilzeilenabstand=5;
ProfilUeberlappungWert=3.2; //0.1
SenkrechterAbstand=5.0; //0.1
Braillezellenabstandzusatz=0.4; //0.1
Grundplatte=true;
Schrift=true;
additional_distance_between_cells=Braillezellenabstandzusatz;
Punktform="sphere"; //["sphere","cylinder"]
Punkt_Durchmesser=2.4;
Punkt_Erhoehung=-0.25;

Grundplattenfarbe="black"; //["grey","black", "blue", "white", "yellow"]
Schriftfarbe="white"; //["white", "yellow", "black", "grey", "blue"]
Profilschriftzylinderhoehe=1.2;

ProfilText = ProfilText_ist_BrailleText_Grosz ? strtoupper(BrailleText) : ProfilText_;
//ProfilText = ProfilText_is_Uppercase_BrailleText ? strtoupper(BrailleText) : ProfilText_;
//ProfilText_is_Uppercase_BrailleText=true; //false|true

use <hershey.scad>;
use <braille.scad>;

// strtoupper is a derivate of function strtolower
// source of function strtolower: CC BY-SA 4.0 https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/String_Functions#str (date of access 2024-05-09)
// Lower case all chars of a string -- does not work with accented characters
// function strtolower (string) = 
//   chr([for(s=string) let(c=ord(s)) c<91 && c>64 ?c+32:c]); 
// Uper case all chars of a string
function strtoupper (string) = 
  chr([for(s=string) let(c=ord(s)) (c<91+32 && c>64+32) || c==252 || c==246 || c==228 ?c-32:c]);

/* echo(strtoupper("hallö äöüß0")); // strtoupper test */
/* echo(str("ö ", ord("ö"))); //246 */
/* echo(str("Ö ", ord("Ö"))); //214 (- 246 214)32 */
/* echo(str("ä ", ord("ä"))); //228 */
/* echo(str("Ä ", ord("Ä"))); //196 (- 228 196)32 */
/* echo(str("ü ", ord("ü"))); //252 */
/* echo(str("Ü ", ord("Ü"))); //220 (- 252 220)32 */


// function for textfield 
function lineranges(s, delimiter="\n")=
  let(linebreakpos=search(delimiter, s,0))
  [0, for(lb=linebreakpos[0])lb, len(s)-1];

// function for textfield 
function split(s,delimiter="\n") =
  let(ranges=lineranges(s=s,delimiter=delimiter))
  is_string(s) && len(ranges)>2 ? (
  [for(a=[0:len(ranges)-2])
      chr([for(pos=[ranges[a]:ranges[a+1]]) 
	      s[pos]==delimiter?0:ord(s[pos])])]) : [s];

// test textfield
module textfieldtest()
{
  text="hello\nworld\ntest";
  lines=split(text);
  echo(str("lines: ",lines));
  for(line=[0:len(lines)-1])
    translate([0,-12*line,0])text(lines[line]);
}
//textfieldtest();


/* Braille */
module Braille(BrailleText, ProfilUeberlappung=false, Schrift=Schrift, Grundplatte=Grundplatte){
  echo(split(BrailleText));
  Zeilen=[for(zeile=split(BrailleText))  braillerow(zeile)];
  BrailleBreite=field_width(Zeilen, additional_distance_between_cells=Braillezellenabstandzusatz);
  echo(BrailleBreite=BrailleBreite);
  BrailleHoehe=field_height(Zeilen);
  echo(BrailleHoehe=BrailleHoehe);
  translate([1/2,0,GrundPlattenStaerke]){
    if(Schrift){color(Schriftfarbe)field(Zeilen, additional_distance_between_cells=Braillezellenabstandzusatz, shape=Punktform, dot_size=Punkt_Durchmesser, raise=Punkt_Erhoehung);}
    //translate([-1/2,0,-GrundPlattenStaerke]){cube([BrailleBreite+1,BrailleHoehe,GrundPlattenStaerke]);}
    minkowskiradius=GrundPlattenStaerke;
    YVersatz=ProfilUeberlappung ? ProfilUeberlappungWert:0;
    translate([-1/2+minkowskiradius,-YVersatz+minkowskiradius,-GrundPlattenStaerke]){
      if(Grundplatte && len(BrailleText)>0){
	color(Grundplattenfarbe)minkowski(){
	  cube([BrailleBreite+1-minkowskiradius*2,BrailleHoehe+YVersatz-minkowskiradius*2,0.06]);
	  //cylinder(h=GrundPlattenStaerke-1, r=1);
	  // minkowski rundung
	  difference(){
	    $fn=12;
	    sphere(r=minkowskiradius);
	    translate([-minkowskiradius,-minkowskiradius,-minkowskiradius*2]){cube([minkowskiradius*2,minkowskiradius*2,minkowskiradius*2]);}
	  }
	}
      }
    }
  }
}

/* Profilschrift */
module Profil(ProfilText, Schrift=Schrift, Grundplatte=Grundplatte, ProfilSchriftgroesze=ProfilSchriftgroesze){
  cylinderradius1=1.4;
  ProfilTextlines = split(ProfilText);
  minkowskiradius=GrundPlattenStaerke;
  translate([1.5,2+1,GrundPlattenStaerke]){
    if(Schrift && len(ProfilText)>0){
      for(zeile=[0:len(ProfilTextlines)-1])
	translate([0,-zeile*(ProfilSchriftgroesze+cylinderradius1*2+Profilzeilenabstand),0])
	  color(Schriftfarbe)drawHersheyText(ProfilTextlines[zeile], font="Sans", size=ProfilSchriftgroesze) cylinder(r1=cylinderradius1,r2=0.2,h=Profilschriftzylinderhoehe,$fn=8);
    }
    ProfilBreite= is_string(ProfilTextlines[0])?max([for(zeile=ProfilTextlines)
			 widthHersheyText(zeile, font="Sans", size=ProfilSchriftgroesze)]
						    ):0;
    echo(ProfilBreite=ProfilBreite);
    /*   translate([-1,-2,-GrundPlattenStaerke]){ */
    /*   cube([ProfilBreite+1,ProfilSchriftgroesze+4,GrundPlattenStaerke]); */
    /* } */

    translate([minkowskiradius-1.5,-(ProfilSchriftgroesze+cylinderradius1*2+Profilzeilenabstand)*(len(ProfilTextlines)-1)-3+minkowskiradius,-GrundPlattenStaerke]){
      if(Grundplatte && len(ProfilText)>0){
	color(Grundplattenfarbe)minkowski(){
	  cube([ProfilBreite+3-minkowskiradius*2,(ProfilSchriftgroesze+cylinderradius1*2+Profilzeilenabstand)*(len(ProfilTextlines))+3-minkowskiradius*2,0.06]);
	  // minkowski rundung
	  difference(){
	    $fn=12;
	    sphere(r=minkowskiradius);
	    translate([-minkowskiradius,-minkowskiradius,-minkowskiradius*2]){cube([minkowskiradius*2,minkowskiradius*2,minkowskiradius*2]);}
	  }
	}
      }
    }
  }
}

/* Oese */
module Oese(){
  color(Grundplattenfarbe)translate([0,0,0]){
    Zeilen=[braillerow(split(BrailleText)[0])];
    BrailleHoehe=field_height(Zeilen);
    radius2=(BrailleHoehe+ProfilSchriftgroesze+SenkrechterAbstand)/2;
    translate([0,radius2+1.5,0]){
      minkowskiradius=GrundPlattenStaerke;
      minkowski(){
	//cylinder(h=GrundPlattenStaerke, r=radius2);
	difference(){
	  cylinder(h=0.06, r=radius2);
	  translate([0,0,-1]){
	    cylinder(h=GrundPlattenStaerke+2, r=radius2-4);
	  }
	}
	// minkowski rundung
	difference(){
	  $fn=12;
	  sphere(r=minkowskiradius);
	  translate([-minkowskiradius,-minkowskiradius,-minkowskiradius*2]){cube([minkowskiradius*2,minkowskiradius*2,minkowskiradius*2]);}
	}
      }
    }
  }
}

translate([0,17+SenkrechterAbstand,0]){
  Braille(BrailleText,ProfilUeberlappung=true);
}
Profil(ProfilText, ProfilSchriftgroesze=ProfilSchriftgroesze);
if(Oese && Grundplatte){Oese();}
