# Define Beschriftung
[OpenSCAD](https://openscad.org) script for 3D printing inscription with Braille and profile font.

[Repository https://gitlab.com/projekt-define/define-beschriftung](https://gitlab.com/projekt-define/define-beschriftung)

[LIESMICH in Deutsch](https://gitlab.com/projekt-define/define-beschriftung/-/blob/master/LIESMICH.md?ref_type=heads)

# Contact
[https://defineblind.at](https://defineblind.at)

[Mail: Info Define info_at_defineblind.at](mailto:Info Define <info_at_defineblind.at>)

# Attribution
BSVWNB/Define

[https://defineblind.at](https://defineblind.at)

# Acknowledgement
This work was developed as part of the [Define project](https://defineblind.at), [Mail: Info Define info_at_defineblind.at](mailto:Info Define <info_at_defineblind.at>).

The supporting organization for the Define project is the [Blinden- und Sehbehindertenverband Wien, Niederösterreich und Burgenland](https://www.blindenverband-wnb.at/) (BSVWNB). 

This project is funded by [AK Vienna within the framework oft the Digitization Fund Work 4.0](https://wien.arbeiterkammer.at/service/digifonds/index.html).

This work uses braille.scad CC-BY-4.0, 2022, Johannes Strelka-Petz, from [whistle braille](https://www.thingiverse.com/thing:5420519)

This work uses "Hershey fonts for OpenSCAD" CC-BY-4.0, 2017, Alexander Pruss

Contributors: Denis Sari, Mario Manseder, Mathias Schmuckerschlag, Erwin Ernst Steinhammer, OStR Prof. Mag. Erich Schmid, Johannes Střelka-Petz

# Copyright & License
    SPDX-FileCopyrightText: 2023 Johannes Strelka-Petz <johannes_at_oskars.org> for BSVWNB/Define
    SPDX-License-Identifier: CERN-OHL-S-2.0+
    ------------------------------------------------------------------------------
    | Copyright 2023 Johannes Strelka-Petz for BSVWNB/Define                       |
    |           2025 Johannes Strelka-Petz                                         |
    |                                                                              |
    | This source describes Open Hardware and is licensed under the CERN-OHL-S v2  |
    | or any later version.                                                        |
    |                                                                              |
    | You may redistribute and modify this source and make products using it under |
    | the terms of the CERN-OHL-S v2 or any later version                          |
    | (https://ohwr.org/cern_ohl_s_v2.txt).                                        |
    |                                                                              |
    | This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,          |
    | INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A         |
    | PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 or any later version        |
    | for applicable conditions.                                                   |
    |                                                                              |
    | Source location: https://gitlab.com/projekt-define/define-beschriftung       |
    |                                                                              |
    | As per CERN-OHL-S v2 section 4, should You produce hardware based on this    |
    | source, You must where practicable maintain the Source Location visible      |
    | on the Oskar Zither PCB or other products you make using this source.        |
     ------------------------------------------------------------------------------

![Example name tags](labels.jpg)
